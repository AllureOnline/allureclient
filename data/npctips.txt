地图名	NPC名字	描述信息
juxianzhen	玄剑真人	青城门派传功长老\n提供青城技能书、发布低级门派任务、管理仓库
juxianzhen	金鼎尊者	蓬莱门派传功长老\n提供蓬莱技能书、发布低级门派任务、管理仓库
juxianzhen	衍星真人	罗浮门派传功长老\n提供罗浮技能书、发布低级门派任务、管理仓库
juxianzhen	工匠学徒	手艺精湛，可以帮你精练装备、修理装备，并且可以进行装备买卖
juxianzhen	药品商	贩卖药品，为你挑战怪物提供补给品
juxianzhen	幽火	帮派管理员，处理各种帮派事务
juxianzhen	东海散人	发布主线任务
juxianzhen	紫月仙子	跑环任务发布者
xinzhen-01	青城派仙逸子	青城门派弟子\n提供青城技能书、管理仓库
xinzhen-01	蓬莱派清心子	蓬莱门派弟子\n提供蓬莱技能书、管理仓库
xinzhen-01	罗浮派玄真子	罗浮门派弟子\n提供罗浮技能书、管理仓库
xinzhen-01	工匠学徒	手艺精湛，可以帮你精练装备、修理装备，并且可以进行装备买卖
xinzhen-01	药品商	贩卖药品，为你挑战怪物提供补给品
xinzhen-01	奖品发放员	发放官方活动的各种奖励
