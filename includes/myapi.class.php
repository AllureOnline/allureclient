<?php
class myapi {
	var $api_secret;
	var $app_id;
	var $api_url;
	
	function myapi($app_id, $api_secret, $api_url = 'www.appsmail.ru/platform/api') {
		$this->app_id = $app_id;
		$this->api_secret = $api_secret;
		if (!strstr($api_url, 'http://')) $api_url = 'http://'.$api_url;
		$this->api_url = $api_url;
	}
	
	function mysign($request_params, $uid, $api_secret) {
		ksort($request_params);
		$params = '';
		foreach ($request_params as $key => $value) {
			$params .= "$key=$value";
		}
		return md5($uid . $params . $api_secret)." >> ".$uid.$params.$api_secret;
	}
	function sign_server_server($valid_keys, $secret_key) {
		ksort($_GET);
		$params = '';
		foreach ($_GET as $key => $value) {
			if(in_array($key, $valid_keys)){
				$params .= "$key=$value";
			}
		}
		return md5($params . $secret_key);
	}
	
	function api($method,$params=false) {
		if (!$params) $params = array(); 
		$params['api_id'] = $this->app_id;
		$params['v'] = '3.0';
		$params['method'] = $method;
		$params['timestamp'] = time();
		$params['format'] = 'json';
		$params['random'] = rand(0,10000);
		ksort($params);
		$sig = '';
		foreach($params as $k=>$v) {
			$sig .= $k.'='.$v;
		}
		$sig .= $this->api_secret;
		$params['sig'] = md5($sig);
		$query = $this->api_url.'?'.$this->params($params);
		$res = file_get_contents($query);
		$ConvertResponse = json_decode($res, true);
		return $ConvertResponse['response'][0];
	}
	
	function params($params) {
		$pice = array();
		foreach($params as $k=>$v) {
			$pice[] = $k.'='.urlencode($v);
		}
		return implode('&',$pice);
	}
}