<?php
class allureEngine{
	
	var $Connect = '';
	
	function allureConnect($Driver, $Username, $Password){
		$this->Connect = odbc_connect("Driver={SQL Server};Server=127.0.0.1;Database=qingcheng;", "WebServer", "6b74561f0ff4387e658a48a46fc1f884");
	}
	
	function clearvars($input){
		return preg_replace("/[^A-z0-9-_\.]/", "", $input);
	}
	
	function allureQuery($Query){
		$Query = odbc_exec($this->Connect, $Query);
		return $Query ? $Query : false ;
	}
	
	function allureArray($Query){
		return odbc_fetch_array($Query);
	}
	
	function allureNums($Query){
		return odbc_num_rows($Query);
	}
	
	function gards_update($base, $user, $params = array()){
		$GardsAPI = array('shortcut'=>'app.game.data.Shortcuts','task'=>'app.game.data.HeroTaskList','friend'=>'app.game.data.FriendList','spell'=>'app.game.data.SpellList','guild'=>'app.game.entity.Guild','hero'=>'app.game.entity.Hero','user'=>'app.game.entity.User','store'=>'app.game.data.Store','depot'=>'app.game.data.Depot');
		if(is_file($this->ServerDir . "localdata/" . $this->clearvars($base) . "/" . $this->clearvars($user) . ".txt")){
			$GetData = simplexml_load_file( $this->ServerDir . "localdata/" . $this->clearvars($base) . "/" . $this->clearvars($user) . ".txt");
		}
		if(!$GetData){
			return false;
		}
		$responseXML = new XMLElement($GardsAPI[$base]);
		foreach($GetData as $val=>$key){
			if(in_array($val, array_keys($params))){
				$responseXML->addChild($val,$params[$val]);
			}else{
				$responseXML->addChild($val,$key);
			}
		}
		$fp = fopen( $this->ServerDir . "localdata/" . $base . "/" . $user . ".txt", "w");
		fwrite($fp, $responseXML->asXML());
		fclose($fp);
		return true;
	}
	
	function gards_insert($base, $user, $params = array()){
		$GardsAPI = array('shortcut'=>'app.game.data.Shortcuts','task'=>'app.game.data.HeroTaskList','friend'=>'app.game.data.FriendList','spell'=>'app.game.data.SpellList','guild'=>'app.game.entity.Guild','hero'=>'app.game.entity.Hero','user'=>'app.game.entity.User','store'=>'app.game.data.Store','depot'=>'app.game.data.Depot');
		$responseXML = new XMLElement($GardsAPI[$this->clearvars($base)]);
		foreach($params as $key=>$val){
				$responseXML->addChild($key, $val);
		}
		$fp = fopen( $this->ServerDir . "localdata/" . $this->clearvars($base) . "/" . $this->clearvars($user) . ".txt", "w");
		fwrite($fp, $responseXML->asXML());
		fclose($fp);
		return true;
	}
	
	function gards_last_id($base, $newID = NULL){
		if($newID){
			$fp = fopen( $this->ServerDir . "localdata/" . $this->clearvars($base) . "/id.txt","w");
			fwrite($fp,$newID);
			fclose($fp);
			return true;
		}
		return file_get_contents( $this->ServerDir . "localdata/" . $this->clearvars($base) . "/id.txt");
	}
	
	function Response($text, $callback = null){
		return $callback ? $this->clearvars($callback) . "(" . json_encode($text) . ");" : $text['status'];
	}	
}
class XMLElement{
	
	var $text, $textContent;
	
	function __construct($text){
		$this->text = $text;
	}
	
	function addChild($Key,$Val){
		$this->textContent .= "\n\t<{$Key}>{$Val}</{$Key}>"; 
	}
	
	function asXML(){
		return "<{$this->text}>{$this->textContent}\n</{$this->text}>";
	}
}