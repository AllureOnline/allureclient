<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Xsolla extends CI_Model {
	public function __construct(){
		parent::__construct();
	}

	public function verifyIpMD5($secretKey, $remoteAddress, $command, $v1, $v2, $v3, $id, $md5){
		$whiteList = array("94.103.26.178","94.103.26.181","159.255.220.241","159.255.220.242", "159.255.220.243","159.255.220.244","159.255.220.245","159.255.220.246", "159.255.220.247","159.255.220.248","159.255.220.249","159.255.220.250", "159.255.220.251","159.255.220.252","159.255.220.253","159.255.220.254");
		if(in_array($remoteAddress, $whiteList) === false){
			return(false);
		}
		$md5Hash = md5($command.$v1.$id.$secretKey);
		if(strtolower($md5Hash) != strtolower($md5)){
			return(false);
		}
		return(true);
	}
	
	public function CheckV1($UserName,$Result = NULL){
		if($Result){
			return mysql_result(mysql_query("SELECT `id` FROM `user` WHERE `login`='".$UserName."'"),0);
		}
		if(mysql_num_rows(mysql_query("SELECT `id` FROM `user` WHERE `login`='".$UserName."'")) > 0){
			return(true);
		}
		return(false);
	}
	
	public function getPayment($v1, $sum, $id){
		$getPayment = mysql_fetch_array(mysql_query("SELECT * FROM `payments` WHERE `code`='".$id."'"));
		if($getPayment){
			return $getPayment;
		}else if(mysql_query("INSERT INTO `payments` (`uid`, `time_unix`, `time_norm`, `tpay`, `count`, `code`) VALUES ('".$v1."', '".time()."', '".date("Y-m-d")."',  '2pay','".$sum."','".$id."');")){
			mysql_query("UPDATE `user` SET `dhr`=`dhr`+'".$sum."' WHERE `id`='".$v1."'");
			return mysql_fetch_array(mysql_query("SELECT * FROM `payments` WHERE `code`='".$id."'"));
		}
		return(false);
	}
	
	public function generateResponseCheck($Response){
		$responseXML = new SimpleXMLElement('<response></response>');
		foreach($Response as $val=>$key){
			$responseXML->addChild($val,$key);
		}
		echo $responseXML->asXML();
	}
}