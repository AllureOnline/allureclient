<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
	<title>404 Страница не найдена</title>
	<link rel="stylesheet" type="text/css" href="/content/user/css/error.css" />
</head>
<body>
	<div id="error_content">
		<div class="error_logo">
			<img src="/content/images/index/logo.png" />
			<p><a href="/index.html" target="_self">На главную</a></p>
		</div>
		<div class="error_text">
			<h4>404 Страница не найдена</h4>
			<h5>Запрашиваемая Вами страница не найдена.</h5>
			<div class="error_text_reason">
				<p>Страница не найдена, возможные причины:</p>
				<a>Вы ошиблись при вводе ссылки</a><br />
				<a>Ссылка больше не существует</a>
			</div>
			<div class="error_link">
				<p>Вы можете перейти в следующие разделы нашего сайта:</p>
				<a href="/index.html" target="_self"><b>Начало:</b> www.alluregame.ru</a><br />
				<a href="/news.html" target="_self"><b>Новости:</b> www.alluregame.ru/news.html</a><br />
				<a href="http://forum.alluregame.ru/" target="_self"><b>Наш Форум:</b> forum.alluregame.ru</a>
			</div>
		</div>
	</div>
</body>
</html>