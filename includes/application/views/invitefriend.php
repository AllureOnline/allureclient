<div class="c-c-c">
	<div class="c-l-t">
		<div class="c-r-t">
			<div class="c-l-b">
				<div class="c-r-b">
					<div id="container">
						<div class="d_img1">
							<img style="position: absolute; z-index: -1;">
							<img style="position: absolute; z-index: -1;">
							<div class="sl2">
								<div class="breadcrumbs">
									<span class="bc-left"></span>
									<a class="bc-first" href="/invitefriend.html">Пригласи друга</a>
									<span class="bc-right"></span>
								</div>
							</div>
						</div>
						<div class="text_div" id="updateDiv">
							<div id="textContainer">
								&nbsp;<br>
								<div style="text-align: center;">
									<b>Уважаемые друзья!</b>
								</div><br>Многие из вас хотели бы пригласить в игру своих друзей, чтобы вместе развиваться, качаться, ходить в подземки и уничтожать общих врагов.<br>&nbsp;<br>Мы предлагаем вам простую возможность сделать это. <a href="/user/personalpage">В личном кабинете</a> у каждого появилась ссылка для друзей (как на картинке).<br>&nbsp;<br>
								<div style="text-align: center;">
									<img src="/content/images/invitefriend/invitefriend.png" alt="" title="">
								</div><br>&nbsp;<br>Администрация Allure Online ценит вашу преданность и готова поддержать вашу инициативу соответствующим вознаграждением. <br><b>За каждого приглашенного в игру друга (реферала) вы будете получать по 50 слитков! А если ваш реферал достигнет 10 уровня, вы получите целых 300 слитков! </b><br>&nbsp;<br><i>Предвидим возможное желание некоторых хитрецов обогатиться таким образом и нарегистрировать кучу мультов. Сразу отметим, что у нас есть достаточно мощные технические средства анализа, которые позволяют гарантированно определять нечестных активистов. Подобные попытки будут жестко караться, вплоть до блокировки основного аккаунта жулика.</i><br>&nbsp;<br>Администрация оставляет за собой право на премодерацию приглашений в течении 7 суток.<br>&nbsp;<br>
								<div style="text-align: center;">
									<img src="/content/images/invitefriend/friends.jpg" alt="" title="">
								</div>
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
</div>

								