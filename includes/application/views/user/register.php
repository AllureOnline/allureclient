
<div class="c-c-c">
<div class="c-l-t">
<div class="c-r-t">
<div class="c-l-b">
<div class="c-r-b">
<div id="container">
	<script type="text/javascript">
	<!--
		$("#content").addClass("reg-content");
	//-->
	</script>
    <h2>Регистрация</h2>

		<div id="registrationError"><?php
		if($SuccessRegister){
			echo'<font color="green"><b>Регистрация прошла успешно!</b></font>';
		}
		echo validation_errors();
		?></div>
        <script type="text/javascript" src="/content/user/js/imposition.validate.js"></script>
	    <form id="registerForm" action="/user/register" method="post" onsubmit="return validateForm();" class="jNice" accept-charset="utf-8">
			<div>
                <input type="hidden" value="<?php echo $this->input->cookie('reffCode'); ?>" name="reffCode" />
                <input type="hidden" value="0" name="promo" />

    			<!-- поля формы регистрации -->
				<div class="reg-fields">

					<span class="reg-label-login"><span>Логин</span></span>
					<input type="text" name="mName" id="Text1" class="autohiding" value="4 - 20 символов" />
					<div class="clear"></div>

					<span class="reg-label-pwd"><span>Пароль</span></span>
					<input ype="text" name="mPassword" id="Text2" class="autohiding"  value="6 - 20 символов" />
				
					<div class="clear"></div>

					<span class="reg-label-pwd-rep"><span>Повторите пароль</span></span>
					<input ype="text" name="mPassword2" id="Text3" class="autohiding"   value="Повторите еще раз"/>
			
					<div class="clear"></div>

					<span class="reg-label-email"><span>E-mail</span></span>
					<input type="text" name="mEmail" id="Text4" class="autohiding" value="ваш@емейл.ру" />
					<div class="clear"></div>

				</div>
				<!--// поля формы регистрации -->

				<!-- пользовательское соглашение -->
				<div class="reg-agreement">
					<input type="checkbox" name="mAgreement" checked="checked" /> С условиями <a href="/laws/useragreement" title="Пользовательское соглашение">пользовательского<br />соглашения</a> и <a href="/laws/gamerules" title="Правила игры">правилами игры</a> ознакомлен<br />и согласен.
				</div>
				<!--// пользовательское соглашение -->

				<div class="centered">
					<!-- сабмит регистрации -->
					<span class="reg-submit">
						<span class="reg-submit-l"></span>
						<span class="reg-submit-c"><span>Зарегистрироваться</span></span>
						<span class="reg-submit-r"></span>
						<input type="submit" class="RegButton" id="submit1" value="" />
					</span>
					<!--// сабмит регистрации -->
				</div>
			</div>
		</form>
		<script type="text/javascript">
			$(document).ready(function() {
				// подсказки в парольных полях
				$("#pwdTitle").focus(function() {
					$(this).css("display", "none");
					$("#Text2").focus();
				});
				$("#Text2").blur(function() {
					if (trim($(this).val()) == "") {
						$("#pwdTitle").css("display", "block").blur();
					}
				});
				$("#pwdTitle2").focus(function() {
					$(this).css("display", "none");
					$("#Text3").focus();
				});
				$("#Text3").blur(function() {
					if (trim($(this).val()) == "") {
						$("#pwdTitle2").css("display", "block").blur();
					}
				});
			});
		</script>

</div>

<div class="clear"></div>
</div></div></div></div>
				</div>