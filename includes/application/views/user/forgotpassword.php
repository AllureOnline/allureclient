<div class="c-c-c">
	<div class="c-l-t">
		<div class="c-r-t">
			<div class="c-l-b">
				<div class="c-r-b">
					<div id="container" class="pass-ret">
						<h2>Восстановление пароля</h2>
						<form method="post" action="/user/forgotpassword" onsubmit="return emailvalidate()" class="retr-form">
							<div>
								<p>Введите e-mail, указанный при регистрации:</p>
								<span class = "custom-input">
									<span>
										<span>
											<input type="text" name="email" class="retr-field" id="emailv" />
										</span>
									</span>
								</span>
								<input type="submit" class="img-btn img-btn-remind" value="" />
							</div>
						</form>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
</div>