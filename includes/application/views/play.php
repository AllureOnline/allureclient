<?php
$Account = array();
if($this->input->cookie('sessionCode')){
	$GetLogin = $this->db->query("SELECT active_session.*, users.* FROM users INNER JOIN active_session ON users.id = active_session.uid WHERE users.sesscode = active_session.session AND sesscode = ?", array($this->input->cookie('sessionCode')));
	if($GetLogin->num_rows() > 0){
		$Account = $GetLogin->row();
	}
}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<title>Allure Online</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script type="text/javascript">
	function SaveCookie(name,value){
		var time=new Date();
		time.setTime(time.getTime()+30*60*60*1000);
		document.cookie=name+"="+value+";expires="+time.toGMTString();
	}
	function GetCookie(name){
		var arrStr = document.cookie.split(";");
		for(var i = 0;i < arrStr.length;i ++){
			var temp = arrStr[i].split("=");
			if(temp[0] == name) 
				return unescape(temp[1]);
		}
	}
</script>

<link type="text/css" rel="stylesheet" href="/content/user/css/client.css" />
<script src="http://code.jquery.com/jquery-1.9.0.js"></script>
</head>
<body style="margin:0;background-color:#19100B;">
	<div id="GameContent" style="background-color:#19100B;">
		<div align="center" id="appletDiv" style="width:100%;">
			<APPLET  archive="allureclient_<?php echo (isset($_GET['dev']) ? 'dev' : 'v' ) . time(); ?>.jar" code="game.GameApplet.class" codebase="http://client.allureonline.ru/" id="currentApplet" name="game" style="width:100%;height:100%;">
				<param name="java_arguments" value="-Xmx512M -XX:+ForceTimeHighResolution">
				<param name="config" value="client_v<?php echo time(); ?>.cfg">
				<param name="userName" value="<?php echo $Account->name; ?>">
				<param name="password" value="<?php echo $Account->sesscode; ?>">
			</APPLET>
		</div>
		<div class="line"></div>
		<div class="control">
			<div class="left">
				<a href="javascript:void(0);" onclick="RestartGame();" class="home" title="Перезапустить игру"></a>
				<div class="ge;"></div>
			</div>
			<div class="right">
				<div class="ger"></div>
				<div class="win_choose">
					<a href="javascript:void(0);" onclick="fullScreen();" id="gameScreen" class="b_box" title="Полный Экран"></a>
				</div>
			<div class="ger"></div>
				<div class="sns">
					<a href="javascript:void(0);" onclick="alert('В разработке');" class="facebook" title="Allure Online на Facebook" target="_blank"></a>
					<a href="javascript:void(0);" onclick="alert('В разработке');" class="twitter" title="Твиттер Allure Online" target="_blank"></a>
					<a href="http://vk.com/allureonlineofficial" class="vk" title="Allure Online в Вконтакте" target="_blank"></a>
				</div>
			</div>
		</div>
	</div>
	<script src="/content/user/js/fullscreen-api-polyfill.js"></script>
	<script type="text/javascript">
		var clientHeight = document.compatMode == "CSS1Compat" ? document.documentElement.clientHeight : document.body.clientHeight;
		var clientWidth = document.compatMode == "CSS1Compat" ? document.documentElement.clientWidth : document.body.clientWidth;
		var screenHeight = screen.height;
		var screenWidth = screen.width;
		function fullScreen(element) {
			if (typeof document.fullscreenEnabled !== undefined) {
				if (document.fullscreenElement) {
					document.getElementById("appletDiv").style.height = (clientHeight-35) + "px";
					document.getElementById("GameContent").style.height = clientHeight + "px";
					document.getElementById("GameContent").style.width = clientWidth + "px";
					document.getElementById("gameScreen").title = "Полный Экран";
					document.getElementById("gameScreen").className = "b_box";
					document.exitFullscreen();
				} else {
					document.getElementById("appletDiv").style.height = (screenHeight-35) + "px";
					document.getElementById("GameContent").style.height = screenHeight + "px";
					document.getElementById("GameContent").style.width = screenWidth + "px";
					document.getElementById("gameScreen").title = "Обычный режим";
					document.getElementById("gameScreen").className = "m_box";
					document.getElementById("GameContent").requestFullscreen();
				}
			}
		}
		function RestartGame(){
			if (typeof document.fullscreenEnabled !== undefined) {
				if (document.fullscreenElement) {
					document.getElementById("appletDiv").style.height = (clientHeight-35) + "px";
					document.getElementById("GameContent").style.height = clientHeight + "px";
					document.getElementById("GameContent").style.width = clientWidth + "px";
					document.getElementById("gameScreen").title = "Полный Экран";
					document.getElementById("gameScreen").className = "b_box";
					document.exitFullscreen();
					window.location = "/index.html";
				}else{
					window.location = "/index.html";
				}
			}
		}
		document.getElementById("appletDiv").style.height = (clientHeight-35) + "px";
		document.getElementById("GameContent").style.height = clientHeight + "px";
	</script>
	<!-- Analytics@Google.com counter -->
	<script type="text/javascript">//<![CDATA[
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-47550200-1', 'allureonline.ru');
	  ga('send', 'pageview');
	//]]></script>
	<!-- //Analytics@Google.com counter -->
	<!-- Rating@Mail.ru counter -->
	<script type="text/javascript">//<![CDATA[
	var _tmr = _tmr || [];
	_tmr.push({id: "2458081", type: "pageView", start: (new Date()).getTime()});
	(function (d, w) {
	   var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true;
	   ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
	   var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
	   if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
	})(document, window);
	//]]></script>
	<!-- //Rating@Mail.ru counter -->
</body>
</html>