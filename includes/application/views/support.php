<div class="c-c-c">
	<div class="c-l-t">
		<div class="c-r-t">
			<div class="c-l-b">
				<div class="c-r-b">
					<div id="container">
						<div class="d_img1">
							<img style="position: absolute; z-index: -1;">
							<img style="position: absolute; z-index: -1;">
							<div class="sl2">
								<div class="breadcrumbs">
									<span class="bc-left"></span>
									<a class="bc-first" href="/support.html">Помощь</a>
									<span class="bc-right"></span>
								</div>
							</div>
						</div>
						<div class="text_div" id="updateDiv">
							<div id="textContainer">
								&nbsp;<br />
								Служба поддержки поможет Вам решить проблемы, связанные с управлением аккаунтом, покупкой слитков или затруднениями технического порядка.<br />
								&nbsp;<br />
								Для начала рекомендуем ознакомиться с разделом <a href="/library">Библиотека</a>, там Вы найдете множество полезной информации. <br />
								&nbsp;<br />
								Если этот раздел не помог Вам&nbsp;решить проблему - можно воспользоваться одним из способов связи:<br />
								<br />
								<ul>
									<li>раздел <a href="http://forum.gardsgame.ru/index.php?/forum/5-%D0%B1%D0%B0%D0%B3%D0%B8-%D0%B8-%D0%BE%D1%88%D0%B8%D0%B1%D0%BA%D0%B8/">"Ошибки игры"</a></li>
								</ul>
								<ul>
									<li>авторизовавшись на сайте, зайти в раздел <a href="/user/support">"Обратная связь"</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
</div>

								