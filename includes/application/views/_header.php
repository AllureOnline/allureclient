<?php
/*
if($this->input->cookie('sessionCode') and $this->input->cookie('sessionExpire') < 604800+time()){
	if(!$this->session->userdata('rememberMe')){
		print_r($this->session->all_userdata());	
		$this->input->set_cookie(array('name'=>'sessionExpire','value'=>'','expire'=>0));
		$this->input->set_cookie(array('name'=>'sessionCode','value'=>'','expire'=>0));
		$this->session->sess_destroy();
	}
}
*/

$Account = array();
if($this->input->cookie('sessionCode')){
	$GetLogin = $this->db->query("SELECT active_session.*, users.* FROM users INNER JOIN active_session ON users.id = active_session.uid WHERE users.sesscode = active_session.session AND sesscode = ?", array($this->input->cookie('sessionCode')));
	if($GetLogin->num_rows() > 0){
		$Account = $GetLogin->row();
	}
}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
	<title>Allure Online - <?php echo $title; ?></title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta property="og:image" content="http://www.allureonline.ru/content/images/index/logo.png" />
	<meta name="description" content="Официальный сайт бесплатной ролевой онлайн игры «Allure Online». Огромный фэнтезийный мир!">
	<meta name="keywords" content="allure, allure game, allure online, allure игра, fantasy, game, gards, gards game, gards online, gards игра, mmorpg, mmorpg игра, online, online игра, online игры, rpg, rpg игры, аллуре гейм, аллюр игра, аллюр онлайн, бесплатная игра, бесплатные онлайн игры, браузерная онлайн игра, во что поиграть, гардс гейм, гардс игра, гардс онлайн, игра, игра для apple, игра для linux, игра для macos, игра для osx, играть, игры, игры бесплатно, игры онлайн, интернет игры, компьютерные игры, лучшие онлайн игры, мморпг, онлайн игра, онлайн игры, онлайн рпг, офис игры, ролевые онлайн игры, рпг, рпг игры, скачать игры, стратегия, фентези">
	
	<link rel="SHORTCUT ICON" href="/favicon.ico">
	<link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico">
	<link href="/content/user/css/general.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="/content/user/css/thickbox.css" />
	<link href="/content/user/lightbox/css/jquery.lightbox-0.5.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" src="/content/common/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript" src="/content/user/js/thickbox.js"></script> 
	<script type="text/javascript" src="/content/user/lightbox/js/jquery.lightbox-0.5.pack.js"></script>          
	<script type="text/javascript" src="/content/user/js/swfobject.js"></script>
	<script type="text/javascript" src="/content/user/js/pngFix/jquery.pngFix.min.js"></script>

	<!-- стилизованные формы -->
	<script type = "text/javascript" src = "/content/user/jnice/jquery.jnice.min.js" ></script>
	<link rel = "stylesheet" type = "text/css" href = "/content/user/jnice/nice-forms.css"></link>

	<!-- скролл рейтинга -->
	<script type="text/javascript" src="/content/user/js/jquery.cycle.js"></script>

	<!-- лайтбокс -->
	<script type="text/javascript" src="/content/user/prettyPhoto/jquery.prettyPhoto.min.js"></script>
	<link href="/content/user/prettyPhoto/prettyPhoto.css" rel="stylesheet" type="text/css" />
	<!-- социальная авторизация -->
	<script src="//ulogin.ru/js/ulogin.js"></script>
	
</head>
<body>  
<?php
$RandomHeader = rand(1,5);
if($RandomHeader >= 5){
	echo'<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="/web/20111007215037oe_/http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" width="100%" height="100%" id="header-flash" align="center">
    <param name="movie" value="/content/flash/webbg'.$RandomHeader.'.swf" />
    <param name="wmode" value="transparent" />
    <embed src="/content/flash/webbg'.$RandomHeader.'.swf" quality="high" width="100%"
     height="100%" name="movie" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" wmode="transparent" /> 
</object>';
}else{
	echo'<div class = "index-bg index-bg-'.$RandomHeader.'"><img style="display:none;" src="/content/images/logo/logo.jpg" width="104" height="109" alt="" title=""></div>';
}
?>
    <div id="curtain-left">
        <div id="curtain-right">
			<div id="wrap">
				<div id="header">
					<a class="logo" href="/" title=""><img src="/content/images/index/logo.png" /></a>
					<ul class="topnav">
						<li class="left-item" isparent="false">
							<a class="JexVisualHyperLinkRedact" id="hmenu1" href="/news.html" target="_self">Новости</a>
						</li>

						<li class="left-item about" isparent="true">
							<a class="JexVisualHyperLinkRedact" id="hmenu2" href="/about/history" target="_self">Об игре</a>
							<ul class='sub'>
								<li class='sub-item'>
									<a href='/about/history' id='hmenu2_1' target='_self' parentId='hmenu2' class='JexVisualHyperLinkRedact'>История</a>
								</li>
								<li class='sub-item'>
									<a href='/about/features' id='hmenu2_2' target='_self' parentId='hmenu2' class='JexVisualHyperLinkRedact'>Особенности</a>
								</li>
								<li class='sub-item'>
									<a href='/about/classes' id='hmenu2_3' target='_self' parentId='hmenu2' class='JexVisualHyperLinkRedact'>Классы</a>
								</li>
							</ul>  
						</li>

						<li class="left-item library" isparent="true">
							<a class="JexVisualHyperLinkRedact" id="hmenu3" href="/library/gameface" target="_self">Библиотека</a>
							<ul class='sub'>
								<li class='sub-item'>
									<a href='/library/gameface' id='hmenu3_1' target='_self' parentId='hmenu3' class='JexVisualHyperLinkRedact'>Интерфейс</a>
								</li>
								<li class='sub-item'>
									<a href='/library/autocombat' id='hmenu3_2' target='_self' parentId='hmenu3' class='JexVisualHyperLinkRedact'>Автобот и Тренировка</a>
								</li>
								<li class='sub-item'>
									<a href='/library/pets'  id='hmenu3_3' target='_self' parentId='hmenu3' class='JexVisualHyperLinkRedact'>Стражи</a>
								</li>
								<li class='sub-item'>
									<a href='/library/friends'  id='hmenu3_4' target='_self' parentId='hmenu3' class='JexVisualHyperLinkRedact'>Друзья</a>
								</li>
								<li class='sub-item'>
									<a href='/library/masters'  id='hmenu3_5' target='_self' parentId='hmenu3' class='JexVisualHyperLinkRedact'>Наставничество</a>
								</li>
								<li class='sub-item'>
									<a href='/library/hotkeys'  id='hmenu3_6' target='_self' parentId='hmenu3' class='JexVisualHyperLinkRedact'>Горячие клавиши</a>
								</li>
								<li class='sub-item'>
									<a href='/library/alliances'  id='hmenu3_7' target='_self' parentId='hmenu3' class='JexVisualHyperLinkRedact'>Альянсы</a>
								</li>
							</ul>  
						</li>

						<li class="play-item">
							<a href="/play.html" title="Играть"></a>
						</li>

						<li class="right-item" isparent="false">
							<a class="JexVisualHyperLinkRedact" id="hmenu4" href="/download.html" target="_self">Файлы</a>
						</li>

						<li class="right-item" isparent="false">
							<a class="JexVisualHyperLinkRedact" id="hmenu5" href="/support.html" target="_self">Помощь</a>
						</li>

						<li class="right-item" isparent="false">
							<a class="JexVisualHyperLinkRedact" id="hmenu6" href="http://forum.allureonline.ru/" target="_blank">Форум</a>
						</li>
					</ul>
				</div>
				<div id="inner">
					<div id="sidebar-left">
						<div class="login<?php echo $Account ? ' hide' : ''; ?>">
							<form id="loginform" method="post" action="/user/login" class="jNice">
								<input type="text" name="login" id="login"/>
								<input type="password" name="password" id="pass" />
								<div style="text-align: left; margin: 0 0 0 22px; height: 30px;">
									<input type="checkbox" name="rememberMe" id="rememberme" checked="checked" value="true" />
									<label for="rememberme" id="remembermelabel">Запомнить меня</label>
								</div>
								<div class="clear"></div>
								<input type="submit" class="submit" value=""/>
								<center>
									<script type="text/javascript">
										document.write('<div id="uLogin886fad1c" data-ulogin="display=small;fields=nickname,sex;verify=1;providers=vkontakte' + /*,odnoklassniki,mailru,facebook*/';hidden=' + /*other*/';redirect_uri=//' + window.location.hostname + '%2Fuser%2Flogin%3Fsocial%3Dtrue"></div>');
									</script>
								</center>
								<a class="registration" href="/user/register">Регистрация</a>
								<a class="forgot-password" href="/user/forgotpassword">Напомнить пароль</a>
							</form>
						</div>
						<?php
						if($Account){
						?>
						<div class="user-panel">
							<div class="title">Личный Кабинет</div>

							<div class="name"><?php echo $Account->name; ?></div>
<!--							<div class="balans">balans</div>
-->
							<ul>
								<li>
									<a href="/user/personalpage">Личный кабинет</a>
								</li>
								<li>
									<a href="/user/personalpage">Личные данные</a>
								</li>
								<li>
									<a href="/user/payhistory">Мои платежи</a>
								</li>
								<li>
									<a href="/user/logout">Выход</a>
								</li>
							</ul>
						</div>
						<?php
						}
						if($this->input->get('c') == 'library'){
						?>
						<div class="module-t">
							<div class="module-b">
								<div class="title">Библиотека</div>
								<ul>
									<li<?php echo $this->input->get('m') == 'autocombat' ? ' class="active"' : ''; ?>>
										<a href="/library/autocombat">Автобот и Тренировка</a>
									</li>
									<li<?php echo $this->input->get('m') == 'hotkeys' ? ' class="active"' : ''; ?>>
										<a href="/library/hotkeys">Горячие клавиши</a>
									</li>
									<li<?php echo $this->input->get('m') == 'masters' ? ' class="active"' : ''; ?>>
										<a href="/library/masters">Наставничество</a>
									</li>
									<li<?php echo $this->input->get('m') == 'gameface' ? ' class="active"' : ''; ?>>
										<a href="/library/gameface">Интерфейс</a>
									</li>
									<li<?php echo $this->input->get('m') == 'alliances' ? ' class="active"' : ''; ?>>
										<a href="/library/alliances">Альянсы</a>
									</li>
									<li<?php echo $this->input->get('m') == 'pets' ? ' class="active"' : ''; ?>>
										<a href="/library/pets">Стражи</a>
									</li>
									<li<?php echo $this->input->get('m') == 'friends' ? ' class="active"' : ''; ?>>
										<a href="/library/friends">Друзья</a>
									</li>
									
								</ul>
							</div>
						</div>
						<?php
						}
						if($this->input->get('c') == 'about'){
						?>
						<div class="module-t">
							<div class="module-b">
								<div class="title">Об игре</div>
								<ul>
									<li<?php echo $this->input->get('m') == 'features' ? ' class="active"' : ''; ?>>
										<a href="/about/features">Особенности</a>
									</li>
									<li<?php echo $this->input->get('m') == 'history' ? ' class="active"' : ''; ?>>
										<a href="/about/history">История</a>
									</li>
									<li<?php echo $this->input->get('m') == 'classes' ? ' class="active"' : ''; ?>>
										<a href="/about/classes">Классы</a>
									</li>
								</ul>
							</div>
						</div>
						<?php
						}
						?>
						<div class="rating hide" id="rating-left">
							<h2><!--Тефия, -->Диона<!--, Рея, Феба, Пандора--></h2>                       
								
							<!-- списки с рейтингом -->
							<div class="rating-list" id="rating-list-left">
								<div>
									<ul>
										<li><img src = "/content/images/index/pic-red.png" width = "22" height = "22" title = "" alt = "" /> •Хаосник• [45]</li>
										<li><img src = "/content/images/index/pic-red.png" width = "22" height = "22" title = "" alt = "" /> pumba [43]</li>
										<li><img src = "/content/images/index/pic-green.png" width = "22" height = "22" title = "" alt = "" /> 3gpaBcTByuTe [40]</li>
										<li><img src = "/content/images/index/pic-red.png" width = "22" height = "22" title = "" alt = "" /> FullCrash [40]</li>
										<li><img src = "/content/images/index/pic-blue.png" width = "22" height = "22" title = "" alt = "" /> Sylik [40]</li>
										<li><img src = "/content/images/index/pic-blue.png" width = "22" height = "22" title = "" alt = "" /> Phenomena [40]</li>
										<li><img src = "/content/images/index/pic-green.png" width = "22" height = "22" title = "" alt = "" /> MaDwr [40]</li>
										<li><img src = "/content/images/index/pic-green.png" width = "22" height = "22" title = "" alt = "" /> Sweetness [40]</li>
										<li><img src = "/content/images/index/pic-green.png" width = "22" height = "22" title = "" alt = "" /> Таис [40]</li>
										<li><img src = "/content/images/index/pic-green.png" width = "22" height = "22" title = "" alt = "" /> Малышка [40]</li>
									</ul>
								</div>
								<div>
									<ul>
										<li><img src = "/content/images/index/pic-blue.png"  width = "22" height = "22" title = "" alt = "" /> Holy_Empire™ [1494]</li>
										<li><img src = "/content/images/index/pic-green.png"width = "22" height = "22" title = "" alt = "" /> F_B_I [652]</li>
										<li><img src = "/content/images/index/pic-green.png"width = "22" height = "22" title = "" alt = "" /> Dark_Empire™ [232]</li>
										<li><img src = "/content/images/index/pic-green.png"width = "22" height = "22" title = "" alt = "" /> Non_Stop [197]</li>
										<li><img src = "/content/images/index/pic-green.png"width = "22" height = "22" title = "" alt = "" /> Легенда [100]</li>
										<li><img src = "/content/images/index/pic-green.png"width = "22" height = "22" title = "" alt = "" /> Федерация [89]</li>
										<li><img src = "/content/images/index/pic-green.png"width = "22" height = "22" title = "" alt = "" /> Элита [77]</li>
										<li><img src = "/content/images/index/pic-green.png"width = "22" height = "22" title = "" alt = "" /> Спарта [73]</li>
										<li><img src = "/content/images/index/pic-green.png"width = "22" height = "22" title = "" alt = "" /> Амазонки [72]</li>
										<li><img src = "/content/images/index/pic-green.png"width = "22" height = "22" title = "" alt = "" /> BrotherHooD [72]</li>
									</ul>
								</div>
							</div>
							<!--// списки с рейтингом -->

							<!-- переключатели рейтинга -->
							<ul class="rating-buttons" id="rating-buttons-left">
								<li class="rb-players selected">Игроки</li>
								<li class="rb-clans">Кланы</li>
							</ul>
							<!--// переключатели рейтинга -->
						</div>
					</div>
					<div id="content">