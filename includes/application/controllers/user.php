<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	public function register(){
		$PostArray['SuccessRegister'] = false;
		$this->load->library('form_validation');
		$this->form_validation->set_rules('mName', 'Логин', 'trim|required|min_length[4]|max_length[20]|is_unique[users.name]');
		$this->form_validation->set_rules('mPassword', 'Пароль', 'trim|required|min_length[6]|max_length[20]|matches[mPassword2]');
		$this->form_validation->set_rules('mPassword2', 'Подтверждением', 'trim|required');
		$this->form_validation->set_rules('mEmail', 'E-mail', 'trim|required|valid_email|is_unique[users.email]');
		
		//Invite System
		if(intval($this->input->get('reffCode')) > 0){
			$cookie = array(
				'name'   => 'reffCode',
				'value'  => $this->input->get('reffCode'),
				'expire' => '86500',
				'path'   => '/'
			);
			$this->input->set_cookie($cookie);
		}
		
		if ($this->form_validation->run()){
			$data = array(
				'name'	=>	$this->input->post('mName'),
				'password'	=>	md5($this->input->post('mPassword')),
				'email'		=>	$this->input->post('mEmail'),
			);
			if(intval($this->input->post('reffCode')) > 0){
				$data['reffCode'] == intval($this->input->post('reffCode'));
			}
			$this->db->insert('users', $data);
			$PostArray['SuccessRegister'] = true;
		}
		
		$this->load->view('_header', array('title'=>'Регистрация'));
		$this->load->view('user/register', $PostArray);
		$this->load->view('_fooder');
		/*
			reffCode:
			promo:0
			mName:1
			mPassword:2
			mPassword2:2
			mEmail:asdasd@f.ry
			mAgreement:on
		*/
	}
	
	public function forgotpassword(){
		$PostArray['SuccessRegister'] = false;
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
		
		if ($this->form_validation->run()){
			$GetUser = $this->db->query("SELECT * FROM users WHERE email = ?", array($this->input->post('email')));
		}
		
		$this->load->view('_header', array('title'=>'Восстановление пароля'));
		$this->load->view('user/forgotpassword', $PostArray);
		$this->load->view('_fooder');
	}
	
	public function personalpage(){
		$this->load->view('_header', array('title'=>'Личный Кабинет'));
		$this->load->view('user/personalpage');
		$this->load->view('_fooder');
	}
	
	public function payhistory(){
		$this->load->view('_header', array('title'=>'Личный Кабинет'));
//		$this->load->view('user/payhistory');
		$this->load->view('_fooder');
	}
	
	
	public function login(){
		$this->load->helper('url');
		
		if($this->input->get('social') == 'true'){
			$sLogin = json_decode(file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']), true);
			$uLogins = $this->db->query("SELECT * FROM ulogin WHERE identity = ?", array($sLogin['identity']));
			
			if($uLogins->num_rows() > 0){
				$uLogin = $uLogins->row();
				$GetUser = $this->db->query("SELECT * FROM users WHERE id = ?", array($uLogin->userid));
			}else{
				$nLinks = array(
					'vkontakte'=>array('id'=>'3888062','key'=>'5gqsKg0q2zk21Y813f8EAID1DAmQWvkL','mail'=>'@vkmessenger.com'),
				);
				// Проверяем логин, использовался он в вк или нет
				$getLoginSocial = $this->db->query("SELECT * FROM users WHERE name = ? AND password = ?", array('vk' . $sLogin['uid'], substr(md5($nLinks[$sLogin['network']]['id'] . '_' . $sLogin['uid'] . '_'.$nLinks[$sLogin['network']]['key']), 0, 6)));
				if($getLoginSocial->num_rows() > 0){
					$getLoginSocial = $getLoginSocial->row();
					$userId = $getLoginSocial->id;
				}else{
					// Регистрируем в users
					$data = array(
						'name'	=>	'vk' . $sLogin['uid'],
						'password'	=>	md5(substr(md5($nLinks[$sLogin['network']]['id'] . '_' . $sLogin['uid'] . '_'.$nLinks[$sLogin['network']]['key']), 0, 6)),
						'email'		=>	'id' . $sLogin['uid'] . $nLinks[$sLogin['network']]['mail'],
					);
					if(intval($this->input->cookie('reffCode')) > 0){
						$data['reffCode'] == intval($this->input->cookie('reffCode'));
					}
					$this->db->insert('users', $data);
					// Id пользователя
					$userId = $this->db->insert_id();
				}
				// Регистрируем через social
				$data = array(
					'userid'=>$userId,
					'network'=>$sLogin['network'],
					'profile'=>$sLogin['profile'],
					'identity'=>$sLogin['identity']
				);				
				$this->db->insert('ulogin', $data);
				// Авторизируемся
				$GetUser = $this->db->query("SELECT * FROM users WHERE id = ?", array($userId));
			}
		}else if($this->input->get('social') != 'true'){
			// Проверяем валиность
			$this->load->library('form_validation');
			$this->form_validation->set_rules('login', 'Логин', 'trim|required');
			$this->form_validation->set_rules('password', 'Пароль', 'trim|required');
			if ($this->form_validation->run()){
				$GetUser = $this->db->query("SELECT * FROM users WHERE name = ? AND password = ?", array($this->input->post('login'), md5($this->input->post('password'))));
			}
			if(!$GetUser){
				redirect(base_url("/index.html"));
				exit();
			}
		}
		
		// Если у нас все корректно то обращяемся в бд
		if($GetUser->num_rows() > 0){
			// Раз мы тут то логин существует...
			$row = $GetUser->row();
			$Autch = time();
			
			// Проверяем старую сессию... если он есть то удаляем...
			$Query = $this->db->query("SELECT * FROM active_session WHERE uid = ?", array($row->id));
			if ($Query->num_rows() > 0){
				$this->db->query("DELETE FROM active_session WHERE uid = ?", array($row->id));
			}
					
			// А тут мы уже вносим новые данные в БД
			$SessionID = md5(($row->name).($row->password).$Autch.$this->input->ip_address().$this->input->user_agent());
			$this->db->insert('active_session', array(
				'uid'=>$row->id,
				'atime'=>$Autch,
				'session'=>$SessionID,
				'uip'=>$this->input->ip_address(),
				'browser'=>$this->input->user_agent()
			));
			// заносим сессию и создаем кукисы...
			$this->db->update('users', array('sesscode'=>$SessionID), "id = '".$row->id."'");
			$this->input->set_cookie(array('name'=>'sessionCode','value'=>$SessionID,'expire'=>(!$this->input->post('rememberMe')? (time()+604800) : (time()+31536000))));
			$this->input->set_cookie(array('name'=>'sessionExpire','value'=>(!$this->input->post('rememberMe')? (time()+604800) : (time()+31536000)),'expire'=>(!$this->input->post('rememberMe')? (time()+604800) : (time()+31536000))));
			$this->session->set_userdata(array('rememberMe'=>TRUE));
		}
		
		if($this->input->post('ReturnUrl')){
			redirect(base_url(urldecode(base64_decode($this->input->post('ReturnUrl')))));
			exit();
		}
		redirect(base_url("/index.html"));
		exit();
	}
	
	public function logout(){
		$this->load->helper('url');
		$this->session->sess_destroy();
		$this->input->set_cookie(array('name'=>'sessionCode','value'=>'','expire'=>0));
		$this->input->set_cookie(array('name'=>'sessionExpire','value'=>'','expire'=>0));
		redirect(base_url("/index.html"));
	}
}