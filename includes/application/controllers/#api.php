<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class api extends CI_Controller {

	public function getonline(){
		$NumRowsOthers = $this->db->query("SELECT * FROM `usersOnline` WHERE `lastUpdate` > '" . (time()-300) . "'");
		$NumRowsUsers = $this->db->query("SELECT * FROM `users` WHERE `last` > '" . (time()-300) . "'");
		exit($this->input->get('callback') ? preg_replace("/[^A-z0-9-_\.]/", "", $this->input->get('callback')) . "(" . json_encode(array("status"=>"200","count"=>($NumRowsOthers->num_rows()+$NumRowsUsers->num_rows()))) . ");" : json_encode(array("status"=>"200","count"=>($NumRowsOthers->num_rows()+$NumRowsUsers->num_rows()))));
	}

	public function sendonline(){
		$Response = array("status"=>"500","comment"=>"error");
		if(!$this->input->cookie('apiSendOnline')){
			$apiSendOnline = time();
			$this->input->set_cookie(array('name'=>'apiSendOnline','value'=>$apiSendOnline,'expire'=>time()+31536000));
			$this->db->insert('usersOnline', array(
					'uIp'			=>	$this->input->ip_address(),
					'userId'		=>	$apiSendOnline,
					'lastUpdate'	=>	time()
				)
			);
			$Response = array("status"=>"200","comment"=>"insert");
		}
		$this->db->update('usersOnline', array('lastUpdate'=>time(),'uIp'=>$this->input->ip_address()), "userId = '" . preg_replace("/[^0-9]/", "", $this->input->cookie('apiSendOnline')) . "'");
		$Response = array("status"=>"200","comment"=>"update");
		
		exit($this->input->get('callback') ? preg_replace("/[^A-z0-9-_\.]/", "", $this->input->get('callback')) . "(" . json_encode($Response) . ");" : json_encode($Response));
	}
}